//
//  Layabout.h
//  HomeWork1
//
//  Created by Andrii Tymchenko on 2/24/16.
//  Copyright © 2016 Andrii Tymchenko. All rights reserved.
//

#import "Hunan.h"

@interface Layabout : Hunan

- (void) moving;

@property(strong,nonatomic) NSString* power;
@property(strong,nonatomic) NSString* capability;

@end
