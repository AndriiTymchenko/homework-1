//
//  AppDelegate.m
//  HomeWork1
//
//  Created by Andrii Tymchenko on 2/24/16.
//  Copyright © 2016 Andrii Tymchenko. All rights reserved.
//

#import "AppDelegate.h"
#import "Hunan.h"
#import "Bicyclist.h"
#import "Runner.h"
#import "Swimmer.h"
#import "Layabout.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    
    Hunan* Human = [[Hunan alloc]init ];
    Bicyclist* Andrii = [[Bicyclist alloc]init ];
    
    //старайся вирівнвати код, так він краще читається і гарніше, маю на увазі так як далі
    
    Runner*     Roman = [[Runner alloc]  init ];
    Swimmer*    Bodya = [[Swimmer alloc] init];
    Layabout*   Tuma  = [[Layabout alloc] init];
    
    Human.name      = @"Kolya";
    [Andrii setName:@"Andrii"];
    Roman.name      = @"Roman";
    [Bodya setName:@"Bodya"];
    Tuma.name       = @"Tuma";
    
    
    Human.height    = 1.8f;
    [Andrii setHeight:1.75f];
    Roman.height    = 2.05f;
    [Bodya setHeight:1.65f];
    Tuma.height     = 999.999999;
    
    Human.weight    = 80.f;
    [Andrii setWeight:75.f];
    Roman.weight    = 120.f;
    [Bodya setWeight:40.f];
    Tuma.weight     = 999.999999;
    
    Human.gender    = @"male";
    [Andrii setGender:@"male"];
    Roman.gender    = @"Мужик";
    [Bodya setGender:@"male"];
    Tuma.gender     = @"Красавчик";
    
    Tuma.power = @"immposible";
    Tuma.capability = @"infinity";
    
    
    //NSArray* array = [[NSArray alloc]initWithObjects:Tuma, Bodya, Roman, Andrii, Human, nil ];
    
    //Скутаренко не показує, але є ще одна корисна штука, називається літерали, вонт існують для швидкого запису
    // масива наприклад http://cocoaheads.tumblr.com/post/17757846453/objective-c-literals-for-nsdictionary-nsarray
    //їх дуже часто просять використовувати замість такого
    //запису: [[NSArray alloc]initWithObjects:Tuma, Bodya, Roman, Andrii, Human, nil ];
    //літерал буде виглядати так:
    
   // NSArray *arrayUsingLiteralEmpty = @[]; //пустий масив
    
   // NSArray *arrayUsingLiteralWithRomaObject = @[Roman];
    
    NSArray *arrayUsingLiteralWithAllObject = @[Tuma, Bodya, Roman, Andrii, Human];
    
    //спробуй вивести цикл який знизу (данні) і для масиву arrayUsingLiteralWithAllObject. Впевнишся що він точно такий самий
    
    
    
       //  for (Hunan* human in arrayUsingLiteralWithAllObject) {
    for (NSInteger index = [arrayUsingLiteralWithAllObject count]-1; index > -1; index--) {
        Hunan* human = [arrayUsingLiteralWithAllObject objectAtIndex:index];
         
         NSLog(@"name = %@,height = %1.1f,weight = %1.1f,gender = %@",human.name, human.height, human.weight, human.gender);
         [human moving];
         
             if ([human isKindOfClass:[Layabout class]]) {
             
             Layabout* tuma = (Layabout*)human;
             
             NSLog(@"power = %@,capabilitys = %@",tuma.power,tuma.capability);
             }
         
         }
     
    

    
    //вкстаті дивись @""   для строк теж літерал =) можна робити двома варіантами (дві строки після коментарія)
    
   // NSString *stringUsingAllocInitMethods = [[NSString alloc] initWithFormat:@"stringUsingAllocInitMethods"];
    
   // NSString *stringUsingLiterals = @"stringUsingLiterals";
    //???????????????????????????
    
    // як бачимо alloc init використовує літерал для своєї роботи, тому записувати як в другому варіанті набагато швидше і краще
    
    
    
   /*
    
    for (Hunan* human in array) {
        
        NSLog(@"name = %@,height = %f,weight = %f,gender = %@",human.name, human.height, human.weight, human.gender);
        [human moving];
        
        if ([human isKindOfClass:[Layabout class]]) {
            
            Layabout* tuma = (Layabout*)human;
            
            NSLog(@"power = %@,capabilitys = %@",tuma.power,tuma.capability);
        }
   
    }
    
*/
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
