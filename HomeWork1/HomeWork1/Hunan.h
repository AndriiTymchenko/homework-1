//
//  Hunan.h
//  HomeWork1
//
//  Created by Andrii Tymchenko on 2/24/16.
//  Copyright © 2016 Andrii Tymchenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>

@interface Hunan : NSObject


@property(strong,nonatomic) NSString* name;
@property(assign,nonatomic) CGFloat height;
@property(assign,nonatomic) CGFloat weight;
@property(strong,nonatomic) NSString*  gender;


- (void) moving;

@end
