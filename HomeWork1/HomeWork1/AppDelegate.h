//
//  AppDelegate.h
//  HomeWork1
//
//  Created by Andrii Tymchenko on 2/24/16.
//  Copyright © 2016 Andrii Tymchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

